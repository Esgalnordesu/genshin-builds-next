import { createSlice, PayloadAction } from '@reduxjs/toolkit'

type ThemeUnion = 'light' | 'dark' | 'system'

const imageHost = process.env.STRAPI_URL || 'http://localhost:1337'

export type SettingsState = {
  theme: ThemeUnion | string
  imageHost?: string
}

const initialState: SettingsState = {
  theme: 'system',
  imageHost,
}

export const settingsSlice = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    setTheme: (state, action: PayloadAction<ThemeUnion | string>) => {
      if (action.payload === 'light') {
        state.theme = action.payload
        localStorage.setItem('theme', action.payload)
        document.documentElement.classList.remove('dark')
      }
      else if (action.payload === 'dark') {
        state.theme = action.payload
        localStorage.setItem('theme', action.payload)
        document.documentElement.classList.add('dark')
      }
      else {
        state.theme = action.payload
        localStorage.setItem('theme', action.payload)
        if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
          document.documentElement.classList.add('dark')
          return
        }
        document.documentElement.classList.remove('dark')
      }
    }
  }
})

export const { setTheme } = settingsSlice.actions

export default settingsSlice.reducer