import { ApolloClient, InMemoryCache, createHttpLink } from "@apollo/client"
import { setContext } from "@apollo/client/link/context"

const authorization = setContext((_, { headers }) => {
  const token = process.env.STRAPI_BEARER
  return {
    headers: {
      ...headers,
      authorization: `Bearer ${token}`
    }
  }
})

const httpLink = createHttpLink({
  uri: `${process.env.STRAPI_URL}/graphql`
})

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: authorization.concat(httpLink),
})

export default client