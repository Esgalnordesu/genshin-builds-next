export const sandsMainStatsDomain = [
  'HP',
  'HP%',
  'DEF',
  'DEF%',
  "Energy Recharge%",
  "ATK",
  "ATK%",
  "Elemental Mastery",
  "Elemental Mastery%",
]

export const circletMainStatsDomain = [
  'HP',
  'HP%',
  'DEF',
  'DEF%',
  "Energy Recharge%",
  "ATK",
  "ATK%",
  "Elemental Mastery",
  "Elemental Mastery%",
  "Crit Rate%",
  "Crit DMG%",
  "Crit Rate/DMG%",
]

export const gobletMainStatsDomain = [
  'HP',
  'HP%',
  'DEF',
  'DEF%',
  "Energy Recharge%",
  "ATK",
  "ATK%",
  "Elemental Mastery",
  "Elemental Mastery%",
  "Pyro DMG Bonus%",
  "Hydro DMG Bonus%",
  "Anemo DMG Bonus%",
  "Electro DMG Bonus%",
  "Dendro DMG Bonus%",
  "Cryo DMG Bonus%",
  "Geo DMG Bonus%",
]

export const substatsDomain = [
  ...sandsMainStatsDomain,
  "Crit Rate%",
  "Crit DMG%",
  "Crit Rate/DMG%",
]
