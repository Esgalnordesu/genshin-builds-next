export function capitalizeAll(target: string, ensureConsistentNaming: boolean = false) {
  const specialNames = ["DPS", "(C6)", "(C5)", "(C4)", "(C3)", "(C2)", "(C1)", "(C0)", "EM"]

  return !ensureConsistentNaming ?
    target
      .split(" ")
      .map(word => `${word.charAt(0).toUpperCase()}${word.substring(1)}`)
      .reduce((curr, next) => `${curr} ${next}`)
      .trimEnd()
    :
    target
      .split(" ")
      .map(word =>
        specialNames.includes(word.toUpperCase())
          ? word.toUpperCase()
          : `${word.charAt(0).toUpperCase()}${word.substring(1)}`
      )
      .reduce((curr, next) => `${curr} ${next}`)
      .trimEnd()
}

export const isURL = (target: string) =>
  !!target.match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)/gm)

export const xssSanification = (target: string) => {
  return target
    .replaceAll(/eval\(.*\)/gm, '')
    .replaceAll(/<script>(.*)<\/script>/gm, '')
    .replaceAll(/function\(.*\)\{.*\}/gm, '')
    .replaceAll(/\(\)\=\>\{\}/gm, '')
    .replaceAll('()', '')
}