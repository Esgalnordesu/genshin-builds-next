export const optionify = (array: any[]) => array.map(e => <option value={e}>{e}</option>)

export function toStrapiType<T, U>(object: T): U {
  return {
    id: 0,
    attributes: { ...object }
  } as U
}