/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: [
      's3.genshinjikken.fun',
      'www.genshinjikken.fun',
      'i.ibb.co'
    ],
  },
}

module.exports = nextConfig
