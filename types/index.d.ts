import { Artifact, Build, Character, Weapon } from "@prisma/client"

export type Paginated<T> = {
  data: T,
  page: number,
  pages: number,
  count: number,
}

export type BuildJoinType = Omit<Build, 'artifactsQuads' | 'artifactsPairs'> & {
  artifactsQuads: Artifact[],
  artifactsPairs: Artifact[],
  equipment: Weapon
}

export type CharacterJoinType = Character & { builds: BuildJoinType[] }

export type StrapiBaseModel<T> = {
  id: number
  attributes: T
}

export type StrapiResponse<T> = {
  data: T
}

export type StrapiPluralResponse<T> = {
  data: T[]
}

export type StrapiGraphQLResponse<T, U extends string> = {
  data: {
    [key: U | string]: StrapiResponse<T>
  }
}

export type StrapiGraphQLPluralResponse<T, U extends string> = {
  data: {
    [key: U | string]: StrapiPluralResponse<T>
  }
}

type Image = {
  name: string;
  hash: string;
  ext: string;
  mime: string;
  path: null;
  width: number;
  height: number;
  size: number;
  url: string;
}

type StrapiMedia = StrapiResponse<
  Partial<
    StrapiBaseModel<{
      name: string
      alternativeText: string
      caption: string
      width: number
      height: number
      formats: {
        thumbnail: Image;
        large: Image;
        small: Image;
        medium: Image;
      }
      hash: string
      ext: string
      mime: string
      size: number
      url: string
      previewUrl: null
      provider: string
      provider_metadata: null
      createdAt: Date
      updatedAt: Date
    }>
  >
>

export type StrapiCharacterBase = {
  createdAt: Date
  updatedAt: Date
  name: string
  bio: string,
  is5Star: boolean
  notes: string
  element: string
  portrait: string
  splash: string
  splashCMS?: StrapiMedia,
  portraitCMS?: StrapiMedia
}

export type StrapiCharacter = {
  id: number,
  attributes: StrapiCharacterBase
}

export type StrapiWeapon = {
  id: number,
  attributes: {
    createdAt: Date
    updatedAt: Date
    name: string
    type: string
    starRating: number
    is5Star: boolean
    subStatValInitial: number
    subStatValFinal: number
    passiveEffect: string
    baseATKInitial: number
    baseATKFinal: number
    subStatType: string
    img: string
    imgCMS: StrapiMedia
  }
}

export type StrapiArtifactsSet = {
  id: number,
  attributes: {
    createdAt: Date
    updatedAt: Date
    name: string
    twoPiecesEffect: string
    fourPiecesEffect: string
    entity_name: null
    img: string
    imgCMS: StrapiMedia
  }
}

export type StrapiCharacterBuild = {
  id: number,
  attributes: {
    createdAt: Date
    updatedAt: Date
    publishedAt: Date
    optimal: boolean
    abilityTips?: string
    talentsPriority?: string
    role: string
    artifactsMainStats?: string
    artifactsSubStats?: string
    weapons: {
      data: StrapiWeapon[]
    },
    artifact_sets_2: {
      data: StrapiArtifactsSet[]
    },
    artifact_sets_4: {
      data: StrapiArtifactsSet[]
    },
  }
}

export type StrapiCharacterJoinType = {
  id: number,
  attributes: StrapiCharacterBase & {
    character_builds: {
      data: StrapiCharacterBuild[]
    }
  }
}