import { gql } from '@apollo/client'
import type { InferGetStaticPropsType, NextPage } from 'next'
import Head from 'next/head'
import { ChangeEvent, useCallback, useState } from 'react'
import Layout from '../../components/Layout'
import WeaponCard from '../../components/WeaponCard'
import client from '../../lib/apollo'
import { StrapiGraphQLPluralResponse, StrapiWeapon } from '../../types'

const Character: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({ data }) => {
  const [workingData, setWorkingData] = useState(data)

  const search = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.value === '') return data
    setWorkingData(
      data.filter(el =>
        el.attributes.name.toLowerCase().includes(e.target.value.toLowerCase()))
    )
  }

  const handleFilter = useCallback((e: ChangeEvent<HTMLSelectElement>) => {
    if (e.target.value === '') return setWorkingData(data)
    setWorkingData(
      data.filter(weapon => weapon.attributes.type === e.target.value)
    )
  }, [data])

  return (
    <Layout>
      <Head>
        <title>GenshinJikken - Weapons</title>
        <meta name="description" content="GenshinJikken" />
        <link rel="icon" href="/favicon.svg" />
      </Head>

      <main className='container mx-auto p-10 bg-slate-50 dark:bg-slate-800 rounded-2xl shadow'>
        <div className="pb-8" >
          <h1 className='text-5xl'>
            Weapons
          </h1>
        </div>
        <div className='flex'>
          <select
            className='bg-slate-100 dark:bg-slate-700 p-4 border-y border-l dark:border-slate-600 w-32 rounded-l-lg text-sm'
            onChange={handleFilter}
          >
            <option value="">All</option>
            <option value="BOW">Bow</option>
            <option value="CATALYST">Catalyst</option>
            <option value="CLAYMORE">Claymore</option>
            <option value="SWORD">Sword</option>
            <option value="POLEARM">Polearm</option>
          </select>
          <input
            className="w-full flex flex-col p-4 mt-4 bg-slate-100 dark:bg-slate-700 rounded-r-lg border dark:border-slate-600 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium"
            type="search"
            placeholder="Filter weapons"
            onChange={search}
          />
        </div>
        {workingData.map(w => (
          <div className="bg-slate-100 dark:bg-slate-700 p-6 my-3 rounded-2xl">
            <WeaponCard weapon={w} />
          </div>
        ))}
      </main>
    </Layout >
  )
}

export async function getStaticProps() {
  const {
    data: {
      weapons: {
        data: data
      }
    }
  }: StrapiGraphQLPluralResponse<StrapiWeapon, 'weapons'> = await client.query({
    query: gql`
    query {
    weapons(sort:"name" pagination:{pageSize:200}) {
      data {
        id
        attributes {
          name
          type
          starRating
          is5Star
          subStatType
          subStatValInitial
          subStatValFinal
          passiveEffect
          baseATKInitial
          baseATKFinal
          img
          imgCMS {
            data {
              attributes {
                url
              }
            }
          }
        }
      }
    }
  }
  `
  })

  return {
    props: {
      data: data,
    },
    revalidate: 60 * 24 // 1 day weapons changes are not too frequent
  }
}

export default Character
