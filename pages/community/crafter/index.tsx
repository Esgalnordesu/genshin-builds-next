import { Artifact, Build, Character, Weapon } from "@prisma/client";
import Head from "next/head";
import { omit } from "radash";
import { useState } from "react";
import useSWR, { Fetcher } from "swr";
import Layout from "../../../components/Layout";
import Portrait from "../../../components/Portrait";
import { circletMainStatsDomain, gobletMainStatsDomain, sandsMainStatsDomain, substatsDomain } from "../../../utils/domains";
import { optionify } from "../../../utils/jsxUtils";
import { xssSanification } from "../../../utils/stringUtils";

type SubmittableBuild = Partial<Build> & { signedBy?: string, name: string }

export default function BuildCrafter() {
  const characterFetcher: Fetcher<Character[], string> = (...args) => fetch(...args).then(res => res.json())
  const artifactsFetcher: Fetcher<Artifact[], string> = (...args) => fetch(...args).then(res => res.json())
  const weaponsFetcher: Fetcher<Weapon[], string> = (...args) => fetch(...args).then(res => res.json())

  const { data: characters } = useSWR<Character[]>('/api/characters', characterFetcher)
  const { data: artifacts } = useSWR('/api/artifacts', artifactsFetcher)
  const { data: weapons } = useSWR('/api/weapons', weaponsFetcher)

  const [currentCharacter, setCurrentCharacter] = useState<Character>()
  const [currentWeapon, setCurrentWeapon] = useState<Weapon>()

  const [selectedArtifacts4, setSelectedArtifacts4] = useState<string>()
  const [selectedArtifacts2, setSelectedArtifacts2] = useState<string[]>(new Array().fill(2))

  // const [teamComp, setTeamComp] = useState<Character[]>(new Array().fill(4))
  const [loading, setLoading] = useState(false)

  const [generatedBuild, setGeneratedBuild] = useState('')

  const [build, setBuild] = useState<SubmittableBuild>({
    abilityTips: '',
    artifactsMainStats: new Array().fill(3),
    artifactsPairs: new Array().fill(2),
    artifactsQuads: [],
    artifactsSubStats: [],
    optimal: false,
    role: '',
    talentsPriority: [],
    isUserGenerated: true,
    name: '',
  })

  const saveUserGeneratedBuild = () => {
    setLoading(true)
    fetch('/api/builds', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        ...omit({
          ...build,
          artifactsPairs: selectedArtifacts2.length > 1 ? selectedArtifacts2 : [],
          artifactsQuads: selectedArtifacts4,
          abilityTips: xssSanification(build.abilityTips ?? ''),
          isUserGenerated: true,
        }, ['id'])
      })
    })
      .then(res => res.json())
      .then(data => setGeneratedBuild(`https://www.genshinjikken.fun/community/builds/${data.id}`))
      .then(() => setLoading(false))
      .then(() => window.scrollTo({
        behavior: 'smooth',
        top: document.body.scrollHeight,
      }))
  }

  return (
    <Layout>
      <Head>
        <title>{`GenshinJikken - BuildsLab`}</title>
        <meta
          name="description"
          content="Create and share your builds with your friends... just with a link"
        />
        <link rel="icon" href="/favicon.svg" />
      </Head>
      <main className="bg-slate-50 dark:bg-slate-800 rounded-2xl">
        <header className="px-8 pt-8 pb-4">
          <h1 className="text-4xl font-bold">
            Build crafter
          </h1>
          <h2 className="text-md font-normal italic">
            Create your own build and team comp and share it with your friends!
          </h2>
        </header>
        <section className="px-8 pt-8 pb-4 columns-1 md:columns-2">
          <label className="block mb-2 text-sm font-medium">
            Name
          </label>
          <input
            type="text"
            placeholder="My super strong build..."
            className="bg-slate-100 border border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => setBuild(state => { return { ...state, name: e.target.value } })}
          />
          <label className="block mb-2 text-sm font-medium">
            Author
          </label>
          <input
            type="text"
            placeholder="Anonymous"
            className="bg-slate-100 border border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => setBuild(state => { return { ...state, signedBy: e.target.value } })}
          />
        </section>
        {/* Role - Character - Weapon -- Selection */}
        <section className="px-8 pt-8 pb-4 columns-1 md:columns-3">
          <label className="block mb-2 text-sm font-medium">
            Role
          </label>
          <input
            type="text"
            placeholder="DPS/Burst Support/Support/Utility..."
            className="bg-slate-100 border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => setBuild(state => { return { ...state, role: e.target.value } })}
          />
          <label className="block mb-2 text-sm font-medium">
            Characters
          </label>
          <select
            className="bg-slate-100 border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => {
              setBuild(state => { return { ...state, characterId: e.target.value } })
              setCurrentCharacter(characters?.find(c => c.id === e.target.value))
            }}>
            {characters?.map(c => (
              <option value={c.id} key={c.id}>
                {c.name}
              </option>
            ))}
          </select>
          <label className="block mb-2 text-sm font-medium">
            Weapons
          </label>
          <select
            className="bg-slate-100 border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => {
              setBuild(state => { return { ...state, weaponId: e.target.value } })
              setCurrentWeapon(weapons?.find(w => w.id === e.target.value))
            }}>
            {weapons?.map(w => (
              <option value={w.id} key={w.id}>
                {w.name}
              </option>
            ))}
          </select>
        </section>
        {/* Artifacts */}
        <section className="px-8 pt-8 pb-4 columns-1 md:columns-3">
          <label className="block mb-2 text-sm font-medium">
            Artifacts (4) (Full-Set)
          </label>
          <select
            className="bg-slate-100 border border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => {
              setSelectedArtifacts4(e.target.value)
            }}>
            {artifacts?.map(a => (
              <option value={a.id} key={a.id}>
                {a.name}
              </option>
            ))}
          </select>
          <label className="block mb-2 text-sm font-medium">
            Artifacts (2) (First set)
          </label>
          <select
            className="bg-slate-100 border border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => {
              setSelectedArtifacts2(state => {
                state[0] = e.target.value
                return [...state]
              })
            }}>
            {artifacts?.map(a => (
              <option value={a.id} key={a.id}>
                {a.name}
              </option>
            ))}
          </select>
          <label className="block mb-2 text-sm font-medium">
            Artifacts (2) (Second set)
          </label>
          <select
            className="bg-slate-100 border border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => {
              setSelectedArtifacts2(state => {
                state[1] = e.target.value
                return [...state]
              })
            }}>
            {artifacts?.map(a => (
              <option value={a.id} key={a.id}>
                {a.name}
              </option>
            ))}
          </select>
        </section>
        {/* Weapon */}
        <section className="px-8 pt-8 pb-4 grid grid-cols-1 md:grid-cols-5 text-center gap-4">
          <Portrait size={96} src={currentCharacter?.portrait || '/defaults/paimon.webp'} />
          <Portrait size={96} src={currentWeapon?.img || '/defaults/paimon.webp'} />
          <Portrait size={96} src={
            (artifacts ?? []).find(a => a.id === selectedArtifacts4)?.img || '/defaults/paimon.webp'
          } />
          <Portrait size={96} src={
            (artifacts ?? []).find(a => a.id === selectedArtifacts2[0])?.img || '/defaults/paimon.webp'
          } />
          <Portrait size={96} src={
            (artifacts ?? []).find(a => a.id === selectedArtifacts2[1])?.img || '/defaults/paimon.webp'
          } />
          <button
            type="button"
            className="text-white bg-gray-600 hover:bg-blue -700 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 mr-2 mb-2 focus:outline-none"
            onClick={() => {
              setBuild(state => { return { ...state, characterId: '' } })
              setCurrentCharacter(undefined)
            }}
          >
            Reset character
          </button>
          <button
            type="button"
            className="text-white bg-gray-600 hover:bg-blue -700 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 mr-2 mb-2 focus:outline-none"
            onClick={() => {
              setBuild(state => { return { ...state, weaponId: '' } })
              setCurrentWeapon(undefined)
            }}
          >
            Reset weapon
          </button>
          <button
            type="button"
            className="text-white bg-gray-600 hover:bg-blue -700 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 mr-2 mb-2 focus:outline-none"
            onClick={() => {
              setSelectedArtifacts4('')
            }}
          >
            Reset artifacts (4)
          </button>
          <button
            type="button"
            className="text-white bg-gray-600 hover:bg-blue -700 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 mr-2 mb-2 focus:outline-none"
            onClick={() => {
              setSelectedArtifacts2(new Array().fill(2))
            }}
          >
            Reset artifacts (2)
          </button>
          <div></div>
        </section>
        {/* Artifacts stats */}
        <section className="px-8 pt-8 pb-4 columns-1 md:columns-3">
          <label className="block mb-2 text-sm font-medium">
            Sands main stat
          </label>
          <select
            className="bg-slate-100 border border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => {
              setBuild(state => {
                const mainStats = [...state.artifactsMainStats ?? []]
                mainStats[0] = e.target.value
                return {
                  ...state, artifactsMainStats: mainStats
                }
              })
            }}>
            {optionify(sandsMainStatsDomain)}
          </select>
          <label className="block mb-2 text-sm font-medium">
            Goblet main stat
          </label>
          <select
            className="bg-slate-100 border border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => {
              setBuild(state => {
                const mainStats = [...state.artifactsMainStats ?? []]
                mainStats[1] = e.target.value
                return {
                  ...state, artifactsMainStats: mainStats
                }
              })
            }}>
            {optionify(gobletMainStatsDomain)}
          </select>
          <label className="block mb-2 text-sm font-medium">
            Circlet main stat
          </label>
          <select
            className="bg-slate-100 border border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => {
              setBuild(state => {
                const mainStats = [...state.artifactsMainStats ?? []]
                mainStats[2] = e.target.value
                return {
                  ...state, artifactsMainStats: mainStats
                }
              })
            }}>
            {optionify(circletMainStatsDomain)}
          </select>
          {/* <label className="block mb-2 text-sm font-medium">
            Artifact substats
          </label>
          <select
            className="bg-slate-100 border border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => {
              setBuild(state => { return { ...state, characterId: e.target.value } })
              setCurrentCharacter(characters?.find(c => c.id === e.target.value))
            }}>
            {optionify(substatsDomain)}
          </select> */}
        </section>
        {/* Tips */}
        <section className="p-8">
          <label className="block mb-2 text-sm font-medium">
            Tips
          </label>
          <textarea
            placeholder="Markdown is allowed!"
            className="bg-slate-100 border border-slate-200 dark:bg-slate-700 border dark:border-slate-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            onChange={(e) => setBuild(state => { return { ...state, abilityTips: e.target.value } })}
          />
        </section>
        <section className="p-8">
          <button
            type="button"
            className="text-white bg-blue-600 hover:bg-blue -700 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 mr-2 mb-2 focus:outline-none"
            disabled={!(build.role && build.characterId && build.weaponId)}
            onClick={saveUserGeneratedBuild}
          >
            Save and share your build!
          </button>
        </section>
        {loading ?
          <section className="pb-8 text-center">
            Please wait...
          </section>
          : null
        }
        {generatedBuild && !loading ?
          <section className="px-8 pb-8">
            <div className="p-4 bg-slate-100 dark:bg-slate-700 rounded-xl w-full">
              <h3 className="text-lg font-semibold mb-3">
                Congrats!
              </h3>
              Your build is reachable from this link
              <div className="mt-3 w-full">
                <code className="bg-slate-50 dark:bg-slate-800 border p-2 rounded w-full">
                  {generatedBuild}
                </code>
              </div>
            </div>
          </section>
          : null
        }
      </main>
    </Layout>
  )
}
