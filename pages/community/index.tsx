import { Character, UserBuild } from "@prisma/client";
import clsx from "clsx";
import Head from "next/head";
import Link from "next/link";
import { chain, list } from "radash";
import { useEffect, useState } from "react";
import useSWR, { Fetcher } from "swr";
import Layout from "../../components/Layout";
import { Paginated } from "../../types";

export default function About() {
  type Response = Paginated<(UserBuild & { character: Character })[]>

  const fetcher: Fetcher<Response, string> =
    (...args) => fetch(...args).then(res => res.json())

  const [builds, setBuilds] = useState<Response>()
  const [currentPage, setCurrentPage] = useState(0)

  const { data } = useSWR('/api/builds?page=0', fetcher)

  useEffect(() => {
    setBuilds(data)
  }, [data])

  const [loading, setLoading] = useState(true)

  const fetchPage = (page: number) => {
    setCurrentPage(page)
    setLoading(true)
    fetch(`/api/builds?page=${page}`)
      .then(res => res.json())
      .then(data => setBuilds(data))
      .then(() => setLoading(false))
    return page
  }

  const fetchPrev = () => chain(
    () => currentPage - 1 <= 0 ? 0 : currentPage - 1,
    fetchPage,
    setCurrentPage,
  )()

  const fetchNext = () => chain(
    () => currentPage + 1 > builds?.pages! ? currentPage : currentPage + 1,
    fetchPage,
    setCurrentPage,
  )()

  useEffect(() => {
    if (builds) setLoading(false)
  }, [builds])

  return (
    <Layout>
      <Head>
        <title>{`GenshinJikken - CommunityBuilds`}</title>
        <meta
          name="description"
          content="Builds created by our users"
        />
        <link rel="icon" href="/favicon.svg" />
      </Head>
      <main className="bg-slate-50 dark:bg-slate-800 md:p-8 rounded-2xl min-h-screen">
        <h1 className="text-4xl p-8 md:p-0 font-bold">
          Community Builds
        </h1>
        <h2 className="mt-2 px-8 md:px-0">
          Explore what the community is doing
        </h2>
        <div className="overflow-x-auto relative sm:rounded-lg mt-8 h-screen">
          <table className="w-full text-sm text-left">
            <thead className="text-xs uppercase bg-slate-200 dark:bg-slate-700 shadow-md">
              <tr>
                <th scope="col" className="py-3 px-6">
                  Build name
                </th>
                <th scope="col" className="py-3 px-6">
                  Author
                </th>
                <th scope="col" className="py-3 px-6">
                  Character
                </th>
                <th scope="col" className="py-3 px-6">
                  Created
                </th>
                <th scope="col" className="py-3 px-6">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {builds?.data?.map((build, idx) => (
                <tr className={idx % 2 === 0 ? "border-b dark:bg-slate-900 dark:border-slate-800" : "border-b dark:bg-slate-700 dark:border-slate-800 bg-slate-200 border-slate-100"}>
                  <th scope="row" className="py-4 px-6 font-medium  whitespace-nowrap dark:text-white">
                    {build.name}
                  </th>
                  <td className="py-4 px-6">
                    {build.signedBy}
                  </td>
                  <td className="py-4 px-6">
                    {build.character.name}
                  </td>
                  <td className="py-4 px-6">
                    {new Date(build.createdAt).toLocaleString()}
                  </td>
                  <td className="py-4 px-6">
                    <Link href={`/community/builds/${build.id}`} className="font-medium text-blue-600 dark:text-blue-500 hover:underline">
                      View
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <div className="text-center mt-6">
            {loading ? 'Loading data...' : ''}
          </div>
        </div>
        <nav className="mx-auto flex items-center justify-center mt-4">
          <ul className="inline-flex -space-x-px">
            <li onClick={fetchPrev}>
              <a href="#" className="py-2 px-3 ml-0 leading-tight text-gray-500 bg-white rounded-l-lg border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Previous</a>
            </li>
            {
              builds ? list(0, builds.pages).map(page => (
                <li onClick={() => fetchPage(page)}>
                  <a href="#" className={clsx(
                    builds.page === page ? 'bg-blue-200' : 'bg-white',
                    "py-2 px-3 leading-tight text-gray-500 border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
                  )}>
                    {page + 1}
                  </a>
                </li>
              )) : null
            }
            <li onClick={fetchNext}>
              <a href="#" className="py-2 px-3 leading-tight text-gray-500 bg-white rounded-r-lg border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Next</a>
            </li>
          </ul>
        </nav>
      </main>
    </Layout>
  )
}