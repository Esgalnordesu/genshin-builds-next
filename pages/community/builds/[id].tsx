import { Artifact, Character, UserBuild, Weapon } from '@prisma/client'
import type { InferGetServerSidePropsType, NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import ReactMarkdown from 'react-markdown'
import AdaptiveBox from '../../../components/AdBox'
import ArtifactCard from '../../../components/ArtifactCard'
import Layout from '../../../components/Layout'
import Portrait from '../../../components/Portrait'
import WeaponCard from '../../../components/WeaponCard'
import { BuildJoinType, StrapiArtifactsSet, StrapiWeapon } from '../../../types'
import { toStrapiType } from '../../../utils/jsxUtils'
import { capitalizeAll } from '../../../utils/stringUtils'

const Character: NextPage<InferGetServerSidePropsType<typeof getServerSideProps>> = ({ data, element }) => {
  const name = capitalizeAll(data.name ?? '')

  return (
    <Layout>
      <Head>
        <title>{`GenshinJikken - ${name}`}</title>
        <meta name="description" content="GenshinJikken" />
        <link rel="icon" href="/favicon.svg" />
      </Head>

      <main className='bg-slate-50 dark:bg-slate-800 md:rounded-2xl pb-2 shadow'>
        <div className="grid grid-cols-1 md:grid-cols-2">
          <section className='px-6 md:px-8'>
            <div className="pt-6 md:pt-8">
              <div className="md:flex md:flex-row">
                <div className="basis-1/6">
                  <Portrait element={element} src={data.character.portrait ?? ''} />
                </div>
                <div className="basis-5/6">
                  <h1 className='text-5xl'>
                    <Link href={`/element/${element}`} className="link">
                      {name}
                    </Link>
                  </h1>
                </div>
              </div>
              <div className='mt-4'>
                <span className="bg-slate-200 dark:bg-slate-700 py-1 px-2 rounded-xl text-xs font-bold">
                  {data.build.role} {data.character.name}
                </span>
                <p className='text-sm italic mt-1'>Build created by {data.signedBy}</p>
                <div className="hidden md:block mt-4">
                  <ReactMarkdown>
                    {data.character.bio ?? ''}
                  </ReactMarkdown>
                </div>
              </div>
            </div>
          </section>
          <figure>
            <img
              src={data.character.splash ?? ''}
              alt=""
              height={350}
              className="w-full"
            />
          </figure>
        </div>

        <div className='md:px-8'>
          <Card build={data.build}></Card>
        </div>
        {data.build.abilityTips ?
          <div className="bg-slate-100 dark:bg-slate-700 p-8 md:mx-8 mb-6 rounded-2xl">
            <h3 className='text-3xl mb-3 font-semibold'>
              Notes
            </h3>
            <ReactMarkdown>
              {data.build.abilityTips ?? '*No notes available*'}
            </ReactMarkdown>
          </div> : null
        }
      </main>
    </Layout>
  )
}

const Card = ({ build }: { build: BuildJoinType }) => {
  return (
    <div id={`build-${build.role}`} className="bg-slate-100 dark:bg-slate-700 p-8 rounded-2xl mb-6">
      <h3 className='text-3xl mb-3 font-semibold'>
        {capitalizeAll(build.role, true)} {build.optimal ? '⭐' : ''}
      </h3>
      <div className="col">
        <WeaponCard weapon={toStrapiType<Weapon, StrapiWeapon>(build.equipment)} />
      </div>
      <div>
        {build.artifactsQuads.map((e: Artifact) => (
          <div className="columns-1" key={e.id}>
            <ArtifactCard artifact={toStrapiType<Artifact, StrapiArtifactsSet>(e)} />
          </div>
        ))}
        <div className='grid grid-cols-1 md:grid-cols-2'>
          {build.artifactsPairs.at(0) ?
            <ArtifactCard
              className="mt-6"
              artifact={
                toStrapiType<Artifact, StrapiArtifactsSet>(
                  build.artifactsPairs.at(0)!
                )
              }
              showOnlyEffect2
              count={2}
            /> : null
          }
          {build.artifactsPairs.at(1) ?
            <ArtifactCard
              className="md:mt-6"
              artifact={
                toStrapiType<Artifact, StrapiArtifactsSet>(
                  build.artifactsPairs.at(1)!
                )
              }
              showOnlyEffect2
              count={2}
            /> : null
          }
        </div>
      </div>
      <div className='md:grid md:grid-cols-3'>
        {
          build.artifactsMainStats.length > 0 ? <div>
            <h3 className="text-2xl my-4">
              Artifacts main stats
            </h3>
            <div>Sands: {build.artifactsMainStats?.at(0) ?? '-'}</div>
            <div>Goblet: {build.artifactsMainStats?.at(1) ?? '-'}</div>
            <div>Circlet: {build.artifactsMainStats?.at(2) ?? '-'}</div>
          </div> : <></>
        }
        {
          build.artifactsSubStats.length > 0 ? <div>
            <h3 className="text-2xl my-4">
              Artifacts sub stats
            </h3>
            {build.artifactsSubStats?.map((ss, idx) => (
              <div>{idx + 1}. {ss ?? '-'}</div>
            ))}
          </div> : <></>
        }
        {
          build.talentsPriority.length > 0 ? <div>
            <h3 className="text-2xl my-4">
              Talents priority (higher to lower)
            </h3>
            {build.talentsPriority?.map((tp, idx) => (
              <div>{idx + 1}. {tp ?? '-'}</div>
            ))}
          </div> : <></>
        }
      </div>
    </div>
  )
}

export async function getServerSideProps({ params }: any) {
  type Response = Omit<UserBuild, 'build'> & {
    build: BuildJoinType,
    character: Character,
  }
  const res = await fetch(`${process.env.ORIGIN}/api/builds/${params.id}`)
  const data: Response = await res.json()

  return {
    props: {
      data: data,
      element: String(data.character.element).toLocaleLowerCase()
    },
  }
}

export default Character
