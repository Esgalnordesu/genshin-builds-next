import Layout from "../components/Layout";

export default function About() {
  return (
    <Layout>
      <main className="bg-slate-50 dark:bg-slate-800 p-8 rounded-2xl">
        <h1 className="text-4xl font-bold">
          About
        </h1>
      </main>
    </Layout>
  )
}