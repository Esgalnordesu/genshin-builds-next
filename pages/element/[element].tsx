import { gql } from '@apollo/client'
import type { InferGetStaticPropsType, NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import GenshinElement from '../../components/GenshinElement'
import Layout from '../../components/Layout'
import Portrait from '../../components/Portrait'
import client from '../../lib/apollo'
import { StrapiCharacter, StrapiGraphQLPluralResponse } from '../../types'
import { ELEMENTS } from '../../utils/constants'
import { capitalizeAll } from '../../utils/stringUtils'

const Element: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({
  data,
  element,
  host,
}) => {
  return (
    <Layout>
      <Head>
        <title>{`GenshinJikken - ${element}`}</title>
        <meta name="description" content="GenshinJikken" />
        <link rel="icon" href="/favicon.svg" />
      </Head>

      <main className="mt-4">
        <div className="text-center flex justify-center gap-3 items-center">
          <h1 className="cursor-pointer font-semibold text-5xl">
            <Link href="/">
              {capitalizeAll(element)}
            </Link>
          </h1>
          <GenshinElement.Element element={element} fixed size={48} />
        </div>

        <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-6 pt-10 gap-4">
          {data.map((x) => (
            <Link href={`/character/${x.id}`} key={x.id}>
              <div
                className="bg-slate-50 dark:bg-slate-800 rounded-2xl border dark:border-slate-700 p-0 cursor-pointer w-full bg-center bg-cover h-96 hover:bg-blue-200 dark:hover:bg-slate-700 hover:duration-150"
                style={{ backgroundImage: `url(${x.attributes.splash ?? `${host}${x.attributes?.splashCMS?.data?.attributes?.url}`})` }}>
                <div className='h-full rounded-2xl hover:bg-slate-100 dark:hover:bg-slate-800 opacity-50 text-center hover:flex items-center justify-center text-transparent hover:text-black dark:hover:text-white hover:duration-150'>
                  <h6 className='text-2xl font-semibold opacity-100'>{capitalizeAll(x.attributes.name)}</h6>
                </div>
              </div>
            </Link>
          ))}
        </div>
      </main>
    </Layout>
  )
}

export async function getStaticPaths() {
  const params = ELEMENTS.map(e => { return { params: { element: e } } })
  return {
    paths: params,
    fallback: false,
  }
}

export async function getStaticProps({ params }: any) {
  const {
    data: {
      genshinCharacters: {
        data: data
      }
    }
  }: StrapiGraphQLPluralResponse<StrapiCharacter, 'genshinCharacters'> = await client.query({
    query: gql`
    query {
      genshinCharacters(
        pagination: { pageSize:200 }
        filters: { element: { eqi: "${params.element}" } }
        sort: "name"
      ) {
        data {
          id
          attributes {
            name
            portrait
            splash
            portraitCMS {
              data {
                attributes {
                  url
                }
              }
            }
            splashCMS {
              data {
                attributes {
                  url
                }
              }
            }
          }
        }
      }
    }
    `
  })

  return {
    props: {
      element: params.element,
      data: data,
      host: process.env.STRAPI_URL,
    },
    revalidate: 1800,
  }
}

export default Element
