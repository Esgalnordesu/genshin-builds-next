import '../styles/globals.scss'

import type { AppProps } from 'next/app'
import Script from 'next/script'
import { Provider } from 'react-redux'
import { store } from '../stores/root'

const clientId = process.env.PUB_ID

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Script
        async
        src={`https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=${clientId}`}
        crossOrigin="anonymous"
      />
      <div className="bg-slate-100 text-gray-700 dark:bg-slate-900 dark:text-slate-100">
        <Component {...pageProps} />
      </div>
    </Provider>
  )
}

export default MyApp
