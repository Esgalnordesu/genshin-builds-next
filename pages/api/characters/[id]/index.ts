import { Artifact } from '@prisma/client'
import type { NextApiRequest, NextApiResponse } from 'next'
import { map } from 'radash'
import prisma from '../../../../lib/primsa'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  const { id, extended } = req.query

  if (req.method === 'GET') {
    try {
      let character: any = await prisma.character.findUnique({
        where: { id: id?.toString() },
        include: { build: true }
      })

      const builds = await prisma.build.findMany({
        where: {
          characterId: id?.toString(),
          isUserGenerated: false,
        },
        include: { equipment: true },
        orderBy: { optimal: 'desc' },
      })

      character.builds = builds

      const quads = new Map<string, (Artifact | null)[]>()
      const pairs = new Map<string, (Artifact | null)[]>()

      if (extended === 'true' && character) {
        for await (const build of character.builds) {
          quads.set(build.id, await map(build.artifactsQuads?.filter(Boolean), async (id: string) => {
            return await prisma.artifact.findUnique({ where: { id } })
          }))
          pairs.set(build.id, await map(build.artifactsPairs?.filter(Boolean), async (id: string) => {
            return await prisma.artifact.findUnique({ where: { id } })
          }))
        }

        character.builds = character.builds.map((b: any) => {
          return {
            ...b,
            artifactsQuads: quads.get(b.id),
            artifactsPairs: pairs.get(b.id),
          }
        })
      }

      return res.status(200).json(character)
    } catch (e) {
      return res.status(500).send(e)
    }
  }
  return res.status(405).send(`Method ${req.method} not allowed`)
}
