import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../lib/primsa'

export default async function handler(
    _: NextApiRequest,
    res: NextApiResponse<any[]>
) {
    const characters = await prisma.character.findMany({
        distinct: ['name'],
        orderBy: {
            name: 'asc'
        },
        select: {
            id: true,
            name: true,
            portrait: true,
            is5Star: true,
            element: true,
        }
    })

    res.status(200).json(characters)
}
