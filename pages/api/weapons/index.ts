import { Weapon } from '@prisma/client'
import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../lib/primsa'

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Weapon | Weapon[] | string>
) {
    const { method } = req

    if (method === 'GET') {
        const weapons = await prisma.weapon.findMany({
            orderBy: {
                name: 'asc'
            }
        })
        res.status(200).json(weapons)
        return
    }
    return res.status(405).send('method not allowed')
}
