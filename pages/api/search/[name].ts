import type { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any[] | string>
) {
  const { name } = req.query

  if (req.method === 'GET') {
    const host = process.env.STRAPI_API
    const apikey = process.env.SEARCH_BEARER
    const endpoint = 'genshin-characters'
    const params = new URLSearchParams({
      'populate[portraitCMS]': '*',
      'fields[0]': 'name',
      'fields[1]': 'portrait',
      'filters[name][$containsi]': String(name),
    })
    try {
      const _res = await fetch(`${host}/${endpoint}?${params}`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${apikey}`
        }
      })
      const data = await _res.json()
      return res.status(200).send(data)
    } catch (e) {
      return res.status(500).send(String(e))
    }
  }
  return res.status(405).send(`Method ${req.method} not allowed`)
}
