import { Artifact } from '@prisma/client'
import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../lib/primsa'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Artifact | Artifact[]>
) {
  const artifacts = await prisma.artifact.findMany({
    orderBy: {
      name: 'asc'
    }
  })
  res.status(200).json(artifacts)
}
