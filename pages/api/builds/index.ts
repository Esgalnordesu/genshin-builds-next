import { Build, UserBuild } from '@prisma/client'
import type { NextApiRequest, NextApiResponse } from 'next'
import { map, omit } from 'radash'
import prisma from '../../../lib/primsa'
import { Paginated } from '../../../types'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any | any[]>
) {
  const { method } = req
  const { page } = req.query

  const PAGE_SIZE = 25

  if (method === 'GET') {
    let builds: any[] = await prisma.userBuild.findMany({
      orderBy: { createdAt: 'desc' },
      include: { build: true },
      take: PAGE_SIZE,
      skip: page ? (Number(page) * PAGE_SIZE) : 0,
    })

    const buildsCount = await prisma.userBuild.count()

    builds = await map(builds, async (build: any) => {
      return {
        ...build,
        character: await prisma.character.findUnique({
          where: { id: build.build.characterId },
          select: { name: true }
        })
      }
    })

    const response: Paginated<UserBuild[]> = {
      data: builds,
      count: buildsCount,
      page: Number(page),
      pages: Math.floor(buildsCount / PAGE_SIZE),
    }

    return res.status(200).json(response)
  }
  if (method === 'POST') {
    const created = await prisma.userBuild.create({
      data: {
        build: {
          create: {
            ...omit(req.body, ['name', 'signedBy']) as Build,
            isUserGenerated: true
          }
        },
        name: req.body.name,
        signedBy: req.body.signedBy,
      }
    })
    return res.status(200).json(created)
  }
  return res.status(405).send('Method not allowed')
}

export default handler