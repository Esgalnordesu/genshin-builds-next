import { Build } from '@prisma/client'
import type { NextApiRequest, NextApiResponse } from 'next'
import { map } from 'radash'
import prisma from '../../../../lib/primsa'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Build[] | Build | string>
) {
  const { method } = req
  const { id } = req.query

  try {
    if (method === 'GET') {
      let build: any = await prisma.userBuild.findUnique({
        include: {
          build: {
            include: {
              equipment: true,
              Character: true,
            }
          }
        },
        where: { id: id?.toString() }
      })

      if (!build) return res.status(404).send('')

      build.character = await prisma.character.findUnique({
        where: { id: build.build.characterId },
        select: {
          name: true,
          element: true,
          portrait: true,
          bio: true,
          splash: true
        }
      })

      const quads = await map(build.build.artifactsQuads?.filter(Boolean), async (id: string) => {
        return await prisma.artifact.findUnique({ where: { id } })
      })
      const pairs = await map(build.build.artifactsPairs?.filter(Boolean), async (id: string) => {
        return await prisma.artifact.findUnique({ where: { id } })
      })

      build.build.artifactsPairs = pairs
      build.build.artifactsQuads = quads

      return build ?
        res.status(200).json(build) :
        res.status(404).send('not found')

    }
  } catch (e) {
    res.status(500).send('???')
  }
  return res.status(405).send('Method not allowed')
}

export default handler