import { gql } from '@apollo/client'
import clsx from 'clsx'
import type { InferGetStaticPropsType, NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import { FC, Fragment, useState } from 'react'
import ReactMarkdown from 'react-markdown'
import ArtifactCard from '../../components/ArtifactCard'
import Layout from '../../components/Layout'
import PlaceHolder from '../../components/PlaceHodler'
import Portrait from '../../components/Portrait'
import WeaponCard from '../../components/WeaponCard'
import client from '../../lib/apollo'
import { StrapiCharacterBuild, StrapiCharacterJoinType, StrapiGraphQLResponse } from '../../types'
import { capitalizeAll } from '../../utils/stringUtils'


const Character: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({
  data,
  element,
  host
}) => {
  const name = capitalizeAll(data.attributes.name ?? '')

  const [showPlaceHolder, setShowPlaceHolder] = useState(false)

  return (
    <Layout>
      <Head>
        <title>{`${name} - Genshin Impact builds - GenshinJikken`}</title>
        <meta name="description" content={`Checkout Genshin Impact ${name}'s latest builds`} />
        <link rel="icon" href="/favicon.svg" />
      </Head>

      <main className={clsx(
        'bg-slate-50 dark:bg-slate-800',
        'md:rounded-2xl',
        'shadow',
        data.attributes.character_builds?.data &&
          data.attributes.character_builds?.data.length > 0
          ? 'pb-2' : ''
      )}>
        <div className="grid grid-cols-1 md:grid-cols-2">
          <section className='px-6 md:px-8'>
            <div className="pt-6 md:pt-8">
              <div className="md:flex md:flex-row">
                <div className="basis-1/6">
                  <Portrait element={element} src={
                    data.attributes.portrait ||
                    `${host}${data.attributes.portraitCMS?.data?.attributes?.url}`
                  }></Portrait>
                </div>
                <div className="basis-5/6">
                  <h1 className='text-5xl'>
                    <Link href={`/element/${element}`} className="link">
                      {name}
                    </Link>
                  </h1>
                </div>
              </div>
              <div className='mt-4'>
                {
                  !data.attributes.character_builds.data ||
                    data.attributes.character_builds.data.length === 0 ?
                    `Currently no available data for ${name} :(` :
                    null
                }
                {data.attributes.character_builds.data.map(build => (
                  <span key={build.attributes.role}>
                    <a
                      href={`#build-${build.attributes.role}`}
                      className="bg-slate-200 dark:bg-slate-700 py-1 px-2 rounded-xl text-xs font-bold"
                    >
                      {capitalizeAll(build.attributes.role, true)}
                    </a>
                    {' '}
                  </span>
                ))}
              </div>
              <div className="hidden md:block mt-8">
                <ReactMarkdown>
                  {data.attributes.bio ?? ''}
                </ReactMarkdown>
              </div>
            </div>
          </section>
          <figure>
            {
              showPlaceHolder ?
                <PlaceHolder.Image width={450} height={300} className="m-4 float-right" /> :
                null
            }
            <img
              src={
                data.attributes.splash ??
                `${host}${data.attributes.splashCMS?.data?.attributes?.url}`
              }
              alt={data.attributes.name}
              height={350}
              className="w-full"
              onError={() => setShowPlaceHolder(true)}
            />
          </figure>
        </div>

        <div className='md:px-8'>
          {data.attributes.character_builds?.data.map((build, idx: number) => (
            <Fragment key={idx}>
              <Card build={build}></Card>
            </Fragment>
          ))}
        </div>

        {data.attributes.notes ?
          <div className="bg-slate-100 dark:bg-slate-700 p-8 md:mx-8 mb-6 rounded-2xl">
            <h3 className='text-3xl mb-3 font-semibold'>
              Notes
            </h3>
            <ReactMarkdown>
              {data.attributes.notes ?? '*No notes available*'}
            </ReactMarkdown>
          </div> : null
        }
      </main>
    </Layout>
  )
}

const Card: FC<{ build: StrapiCharacterBuild }> = ({ build }) => {
  return (
    <div id={`build-${build.attributes.role}`} className="bg-slate-100 dark:bg-slate-700 p-8 rounded-2xl mb-6">
      <h3 className='text-3xl mb-3 font-semibold'>
        {capitalizeAll(build.attributes.role, true)} {build.attributes.optimal ? '⭐' : ''}
      </h3>
      <div className="mb-4 border-b dark:border-b-slate-600">
        {build.attributes.weapons.data.map(weapon => (
          <WeaponCard key={weapon.id} weapon={weapon} />
        ))}
      </div>
      <div >
        {build.attributes.artifact_sets_4.data.map((e) => (
          <div className="grid grid-cols-1" key={e.id}>
            <ArtifactCard
              artifact={e}
              count="4"
            />
          </div>
        ))}
        <div className='grid grid-cols-1 md:grid-cols-2 border-b dark:border-b-slate-600 mb-2'>
          {build.attributes.artifact_sets_2.data.map((e) => (
            <ArtifactCard
              className="mt-4"
              artifact={e}
              showOnlyEffect2
              count="2"
              key={e.id}
            />
          ))
          }
        </div>
      </div>
      <div className='grid grid-cols-1 md:grid md:grid-cols-3 mt-2'>
        {
          build.attributes.artifactsMainStats ? <div>
            <h3 className="text-2xl my-4">
              Artifacts main stats
            </h3>
            <ReactMarkdown>
              {build.attributes.artifactsMainStats}
            </ReactMarkdown>
          </div> : <></>
        }
        {
          build.attributes.artifactsSubStats ? <div>
            <h3 className="text-2xl my-4">
              Artifacts sub stats
            </h3>
            {build.attributes.artifactsSubStats?.split(',').map((ss, idx) => (
              <div>{idx + 1}. {ss?.trim() ?? '-'}</div>
            ))}
          </div> : <></>
        }
        {
          build.attributes.talentsPriority ? <div>
            <h3 className="text-2xl my-4">
              Talents priority (higher to lower)
            </h3>
            {build.attributes.talentsPriority?.split(',').map((tp, idx) => (
              <div>{idx + 1}. {tp?.trim() ?? '-'}</div>
            ))}
          </div> : <></>
        }
      </div>
      <div className='grid grid-cols-1'>
        {
          build.attributes.abilityTips ? <div>
            <h3 className="text-2xl mt-4 mb-3">
              Tips
            </h3>
            <ReactMarkdown>
              {build.attributes.abilityTips}
            </ReactMarkdown>
          </div> : <></>
        }
      </div>
    </div>
  )
}

export async function getStaticPaths() {
  const {
    data: {
      genshinCharacters: {
        data: data
      }
    }
  } = await client.query({
    query: gql`
    query {
      genshinCharacters(pagination: {pageSize: 200}) {
        data {
          id
          attributes {
            element
          }
        }
      }
    }
    `
  })

  const params = data.map((c: any) => { return { params: { id: String(c.id) } } })

  return {
    paths: params,
    fallback: false,
  }
}

export async function getStaticProps({ params }: any) {
  const {
    data: {
      genshinCharacter: {
        data: data
      }
    }
  }: StrapiGraphQLResponse<StrapiCharacterJoinType, 'genshinCharacter'> = await client.query({
    query: gql`
    query {
      genshinCharacter(id: ${params.id}) {
        data {
          attributes {
            name
            bio
            notes
            element
            portrait
            splash
            portraitCMS {
              data {
                attributes {
                  url
                }
              }
            }
            splashCMS {
              data {
                attributes {
                  url
                }
              }
            }
            character_builds {
              data {
                attributes {
                  optimal
                  role
                  artifactsMainStats
                  artifactsSubStats
                  talentsPriority
                  abilityTips
                  weapons {
                    data {
                      attributes {
                        name
                        type
                        baseATKInitial
                        baseATKFinal
                        subStatType
                        subStatValInitial
                        subStatValFinal
                        passiveEffect
                        img
                        imgCMS {
                          data {
                            attributes {
                              url
                            }
                          }
                        }
                      }
                    }
                  }
                  artifact_sets_2 {
                    data {
                      attributes {
                        name
                        twoPiecesEffect
                        fourPiecesEffect
                        img
                        imgCMS {
                          data {
                            attributes {
                              url
                            }
                          }
                        }
                      }
                    }
                  }
                  artifact_sets_4 {
                    data {
                      attributes {
                        name
                        twoPiecesEffect
                        fourPiecesEffect
                        img
                        imgCMS {
                          data {
                            attributes {
                              url
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }    
    `
  })

  return {
    props: {
      data: data,
      element: String(data.attributes.element).toLocaleLowerCase(),
      host: process.env.STRAPI_URL
    },
    revalidate: 900,
  }
}

export default Character