import { gql } from '@apollo/client'
import type { InferGetStaticPropsType, NextPage } from 'next'
import Head from 'next/head'
import { ChangeEvent, useCallback, useState } from 'react'
import ArtifactCard from '../../components/ArtifactCard'
import Layout from '../../components/Layout'
import client from '../../lib/apollo'
import { StrapiArtifactsSet, StrapiGraphQLPluralResponse } from '../../types'

const Character: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({ data }) => {
  const [workingData, setWorkingData] = useState(data)

  const search = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setWorkingData(
      workingData.filter(el =>
        el.attributes.name.toLowerCase().includes(e.target.value.toLowerCase()))
    )
  }, [])

  return (
    <Layout>
      <Head>
        <title>GenshinJikken - Artifacts</title>
        <meta name="description" content="GenshinJikken" />
        <link rel="icon" href="/favicon.svg" />
      </Head>

      <main className='container mx-auto p-10 bg-slate-50 dark:bg-slate-800 rounded-2xl shadow'>
        <div className="row">
          <div className="pb-8" >
            <h1 className='text-5xl'>
              Artifacts
            </h1>
          </div>
          <div>
            <input
              className="w-full flex flex-col p-4 mt-4 bg-slate-100 dark:bg-slate-700 rounded-lg border dark:border-slate-600 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0"
              type="search"
              placeholder="Filter artifacts"
              onChange={search}
            />
          </div>
          {workingData.map(a => (
            <div className="bg-slate-100 dark:bg-slate-700 p-6 my-3 rounded-2xl">
              <ArtifactCard artifact={a} />
            </div>
          ))}
        </div>
      </main>
    </Layout >
  )
}

export async function getStaticProps() {
  const {
    data: {
      artifactSets: {
        data: data
      }
    }
  }: StrapiGraphQLPluralResponse<StrapiArtifactsSet, 'artifactSets'> = await client.query({
    query: gql`
    query {
      artifactSets(sort:"name" pagination:{pageSize:200}) {
        data {
          id
          attributes {
            name
            twoPiecesEffect
            fourPiecesEffect
            img
            imgCMS {
              data {
                attributes {
                  url
                }
              }
            }
          }
        }
      }
    }
  `
  })

  return {
    props: {
      data: data,
    },
    revalidate: 60 * 24 // 1 day weapons changes are not too frequent
  }
}

export default Character
