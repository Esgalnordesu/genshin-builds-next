import type { InferGetStaticPropsType, NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import GenshinElement from '../components/GenshinElement'
import Layout from '../components/Layout'
import { StrapiBaseModel, StrapiPluralResponse } from '../types'
import { ELEMENTS } from '../utils/constants'

const Home: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({ data }) => {
  return (
    <Layout footer>
      <Head>
        <title>GenshinJikken</title>
        <meta
          name="description"
          content="GenshinJikken - The first community-driven Genshin Impact's characters build website!"
        />
        <link rel="icon" href="/favicon.svg" />
      </Head>

      <main className='flex flex-row gap-4'>
        <aside className='md:basis-3/4'>
          <article className='rounded-xl bg-slate-50 dark:bg-slate-800 container mx-auto p-8'>
            <div>
              <h1 className="text-4xl font-bold">Welcome to GenshinJikken</h1>
              <h2 className="mt-8 font-normal">
                The first community-driven Genshin Impact's characters build website!.
              </h2>
              <h3 className='mt-2 mb-4'>
                Use BuildsLab to create your own builds and share them with your friends simply with
                a link. Explore what crazy ideas other users may had in the Community section.
              </h3>
            </div>
            <div>
              <p>
                Select an element to begin... or use the search function above.
              </p>
              <span className="font-normal">
                Or explore the&nbsp;
              </span>
              <Link href="/weapons">
                <span className='font-bold text-md cursor-pointer underline'>weapons</span>
              </Link>
              <span>&nbsp;or&nbsp;</span>
              <Link href="/artifacts">
                <span className='font-bold text-md cursor-pointer underline'>artifacts</span>
              </Link>
              <span>&nbsp;pages</span>
            </div>
            <div className="mt-12 grid grid-cols-3 sm:grid-cols-5 md:grid-cols-7 text-center">
              {ELEMENTS.map((el: any) => (
                <Link
                  key={el}
                  href={`/element/${el}`}
                  className="mx-auto p-2 hover:duration-150 hover:bg-blue-100 dark:hover:bg-gray-700 rounded cursor-pointer"
                >
                  <GenshinElement.Element element={el} hover />
                </Link>
              ))}
            </div>

          </article>
          <article className='rounded-xl bg-slate-50 dark:bg-slate-800 container mx-auto p-8 mt-6 h-64'>
            <div>
              <h3 className="text-4xl font-bold">Want to contribute?</h3>
              <p className="mt-8 text-1xl font-normal">
                Write us... Or contribute!
              </p>
              <div className="mt-6">
                {/* <button
                  type="button"
                  className="text-white bg-orange-600 hover:bg-orange-700 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 mr-2 mb-2 focus:outline-none"
                  onClick={() => window.open('https://gitlab.com/btsstanner/genshin-builds-next')}
                >
                  <img src="https://about.gitlab.com/images/press/press-kit-icon.svg" alt="" />
                  GitLab
                </button> */}
              </div>
            </div>
          </article>
        </aside>
        <aside className='basis-1/4 hidden md:block'>
          <article className='rounded-xl bg-slate-50 dark:bg-slate-800 container mx-auto p-8 h-full'>
            <h3 className="text-4xl font-bold">News</h3>
            <div className='mt-8'>
              {data.data.map(el => (
                <div className='hover:text-lg hover:duration-100 hover:text-blue-400 dark:hover:text-blue-200'>
                  <a href={`/character/${el.attributes.genshin_character.data.id}`}>
                    <span className='font-semibold'>
                      {el.attributes.genshin_character.data.attributes.name}
                    </span>
                    <span className='text-sm'>
                      {' - '}{el.attributes.role} build
                    </span>
                  </a>
                </div>
              ))}
              <img src="/misc/paimon_slime.webp" />
            </div>
          </article>
        </aside>
      </main>
    </Layout>
  )
}

export async function getStaticProps() {
  const params = new URLSearchParams({
    'pagination[pageSize]': '10',
    'sort[0]': 'updatedAt:desc',
    'populate[0]': 'genshin_character',
    'fields[0]': 'role',
  })

  const res = await fetch(`${process.env.STRAPI_API}/character-builds?${params}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${process.env.STRAPI_BEARER}`
    }
  })
  const data: StrapiPluralResponse<StrapiBaseModel<any>> = await res.json()

  return {
    props: {
      data
    },
    revalidate: 600
  }
}

export default Home
