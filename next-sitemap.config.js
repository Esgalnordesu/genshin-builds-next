/** @type {import('next-sitemap').IConfig} */

module.exports = {
  siteUrl: 'https://www.genshinjikken.fun',
  generateRobotsTxt: true,
  changefreq: 'weekly',
  autoLastmod: true,
  generateIndexSitemap: false,
  transform: async (_, path) => {
    return {
      loc: path,
      priority: calculatePriority(path)
    }
  }
}

const priorityOne = [
  '/',
]
const priorityPointEight = [
  '/element/dendro',
  '/element/pyro',
  '/element/hydro',
  '/element/anemo',
  '/element/cryo',
  '/element/electro',
  '/element/geo',
]
const priorityPointSeven = [
  '/about',
  '/artifacts',
  '/community',
  '/crafter',
  '/weapons',
]

function calculatePriority(path) {
  if (priorityOne.includes(path)) return "1.0"
  if (priorityPointEight.includes(path)) return 0.9
  if (priorityPointSeven.includes(path)) return 0.8
  return 0.7
}