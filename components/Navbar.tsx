import Link from "next/link"
import { debounce } from "radash"
import { ChangeEvent, FC, Fragment, useState } from "react"
import { useSelector } from "react-redux"
import { RootState } from "../stores/root"
import { StrapiCharacter, StrapiPluralResponse } from "../types"
import { capitalizeAll } from "../utils/stringUtils"
import Portrait from "./Portrait"
import Primogem from "./Primogem"

const Navbar = () => {
  const [open, setOpen] = useState(false)
  const [result, setResult] = useState<StrapiPluralResponse<StrapiCharacter>>()

  const settings = useSelector((state: RootState) => state.settings)

  const search = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.value.length > 1) {
      fetch(`/api/search/${event.target.value}`)
        .then(res => res.json())
        .then(data => setResult(data))
      return
    }
    setResult(undefined)
  }

  const handleSearch = debounce({ delay: 250 }, search)

  const Result = ({ entry }: { entry: StrapiCharacter }) => {
    return (
      <div
        onClick={() => setResult(undefined)}
        className="container mx-auto bg-slate-50 hover:bg-slate-300 dark:bg-slate-800 px-8 dark:hover:bg-slate-700 rounded-lg cursor-pointer hover:duration-150"
      >
        <Link href={`/character/${entry.id}`}>
          <div className="columns-3 md:columns-12">
            <div>
              <Portrait src={
                entry.attributes.portrait ??
                `${settings.imageHost}${entry.attributes.portraitCMS?.data?.attributes?.url}`
              }></Portrait>
            </div>
            <div className="font-semibold">
              {capitalizeAll(entry.attributes.name)}
            </div>
          </div>
        </Link >
      </div >
    )
  }

  const NavLink: FC<{ href: string, children: any }> = ({ href, children }) => (
    <li className="cursor-pointer underline md:no-underline dark:md:hover:text-blue-100">
      <Link href={href} className="block py-2 pr-4 pl-3 rounded md:bg-transparent hover:text-blue-400 dark:md:text-slate-50 md:p-0 dark:md:hover:text-blue-200" aria-current="page">
        {children}
      </Link>
    </li>
  )

  return (
    <Fragment>
      <nav className="bg-slate-50 dark:bg-slate-800 border-slate-200 px-2 sm:px-4 py-2.5">
        <div className="container flex flex-wrap justify-between items-center mx-auto">
          <a href="/" className="flex items-center">
            <Primogem className="mr-2 h-6 sm:h-9" size={32} />
            <span className="ml-0 self-center text-xl font-semibold whitespace-nowrap">
              GenshinJikken
            </span>
          </a>
          <button
            onClick={() => setOpen(state => !state)}
            data-collapse-toggle="navbar-default"
            type="button"
            className="inline-flex items-center p-2 ml-3 text-sm text-gray-600 dark:text-slate-500 rounded-lg md:hidden hover:bg-slate-600 focus:outline-none focus:ring-2 focus:ring-slate-200"
            aria-controls="navbar-default"
            aria-expanded="false"
          >
            <span className="sr-only">Open main menu</span>
            <svg className="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd" />
            </svg>
          </button>
          <div className={`${open ? '' : 'hidden'} w-full md:block md:w-auto`} id="navbar-default">
            <input
              className="text-center w-full md:w-96 flex flex-col p-4 mt-4 bg-slate-200 dark:bg-slate-700 rounded-lg border md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 "
              type="text"
              placeholder="Search character"
              onChange={handleSearch}
            />
          </div>
          <div className={`${open ? '' : 'hidden'} w-full md:block md:w-auto`} id="navbar-default">
            <ul className="flex flex-col p-4 mt-4 rounded-lg border border-transparent md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0">
              <NavLink href="/community">
                Community
              </NavLink>
              <NavLink href="/community/crafter">
                BuildsLab
              </NavLink>
            </ul>
          </div>
        </div>
      </nav>
      <div className="gridrounded-b-xl w-full mx-auto bg-slate-50 dark:bg-slate-800">
        {result?.data?.map(r => <Result key={r.id} entry={r} />)}
      </div>
    </Fragment>
  )
}

export default Navbar
