import { ChangeEvent } from "react"
import { useDispatch, useSelector } from "react-redux"
import { setTheme } from "../features/settingsSlice"
import { RootState } from "../stores/root"

export const ThemeToggler = () => {
  const settings = useSelector((state: RootState) => state.settings)
  const dispatch = useDispatch()

  const toggleTheme = (e: ChangeEvent<HTMLSelectElement>) => {
    dispatch(setTheme(e.target.value))
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }

  return (
    <select
      className="appearance-none p-2 bg-slate-100 dark:bg-slate-700 rounded"
      onChange={toggleTheme}
      defaultValue={settings.theme}
    >
      <option value="" disabled>Select Theme</option>
      <option value="light">Light 😎</option>
      <option value="dark">Dark 🌙</option>
      <option value="system">System 🖥️</option>
    </select>
  )
}