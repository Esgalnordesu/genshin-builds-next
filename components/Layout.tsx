import { FC, useEffect } from "react"
import { useDispatch } from "react-redux"
import { setTheme } from "../features/settingsSlice"
import Footer from "./Footer"
import Loader from "./Loader"
import Navbar from "./Navbar"

type Props = {
  children: JSX.Element | JSX.Element[],
  footer?: boolean,
  parallax?: boolean,
}

const Layout: FC<Props> = ({ children, footer = true, parallax }) => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setTheme(localStorage.getItem('theme') || 'system'))
  }, [])


  return (
    <main className={parallax ? "parallax min-h-screen" : "min-h-screen"}>
      <Navbar />
      <div className="container mx-auto md:px-16 pt-8 min-h-screen">
        <Loader />
        {children}
      </div>
      <div className="pt-6">
        {footer ? <Footer /> : null}
      </div>
    </main>
  )
}

export default Layout