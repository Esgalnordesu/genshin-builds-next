const Primogem = ({ size, className = "" }: { size: number, className: string }) => {
    return (
        <img
            className={className}
            src="/misc/primogem.webp"
            height={size}
            alt="genshin impact primogem"
        />
    )
}

export default Primogem