import clsx from "clsx"
import Image from "next/image"
import { FC } from "react"
import GenshinElement from "./GenshinElement"

type Props = {
  size?: number
  className?: string
  src?: string
  element?: string
}

const Portrait: FC<Props> = ({ src, size = 72, className = "", element }) => {
  return (
    <div className="relative inline-block">
      <img
        alt=""
        src={src ?? ''}
        className={clsx("bg-slate-200 dark:bg-slate-900", "rounded-lg", className)}
        height={size}
        width={size}
      />
      {element ?
        <div className="transform leading-none absolute top-0 right-0 bg-slate-900 p-1 rounded-full border-slate-700 ring-slate-600 ring-1">
          <GenshinElement.Element element={element} size={15} fixed />
        </div> : null
      }
    </div>
  )
}

export const SimplePortrait: FC<Props> = ({ src = "", size = 72, className = "" }) => {
  return (
    <img
      src={src}
      className={clsx("", "rounded-lg", className)}
      height={size}
      width={size}
      loading="lazy"
    />
  )
}

export default Portrait
