import { snake } from "radash"
import { FC } from "react"
import ReactMarkdown from "react-markdown"
import { StrapiArtifactsSet } from "../types"
import { capitalizeAll } from "../utils/stringUtils"
import { SimplePortrait } from "./Portrait"

const imgHost = process.env.STRAPI_URL

type Props = {
  artifact: StrapiArtifactsSet,
  showOnlyEffect2?: boolean,
  className?: string,
  additionalText?: string
  count?: number | string,
}

const ArtifactCard: FC<Props> = ({
  artifact,
  showOnlyEffect2 = false,
  className = "",
  additionalText = "",
  count,
}) => {
  return (
    <div id={`${snake(artifact.attributes.name.toLowerCase())}`} className={className}>
      <h3 className="text-2xl">
        {capitalizeAll(artifact.attributes.name, true)}
        <span className="text-base">
          {count ? ` (${count})` : ''}
        </span>
        <span className="text-xs">
          {` ${additionalText}`}
        </span>
      </h3>
      <div className="md:flex md:flex-row pt-5 pb-6 ">
        <div className="basis-1/12 mt-1 grow">
          <a href={`/artifacts#${snake(artifact.attributes.name.toLowerCase())}`} style={{ cursor: 'pointer' }}>
            <SimplePortrait
              src={
                artifact.attributes.img ??
                `${imgHost}${artifact.attributes?.imgCMS?.data?.attributes?.url}`
              }
              size={72}
              className="bg-slate-200 dark:bg-slate-800"
            />
          </a>
        </div>
        <div className="basis-4/12 grow">
          <h5 className="text-xl font-semibold">
            Effect (2)
          </h5>
          <ReactMarkdown>
            {artifact.attributes.twoPiecesEffect ?? ''}
          </ReactMarkdown>
        </div>
        {showOnlyEffect2 ? null :
          <div className="basis-7/12 grow">
            <h5 className="text-xl font-semibold">
              Effect (4)
            </h5>
            <ReactMarkdown>
              {artifact.attributes.fourPiecesEffect ?? ''}
            </ReactMarkdown>
          </div>
        }
      </div>
    </div >
  )
}

export default ArtifactCard