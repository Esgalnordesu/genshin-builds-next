import { snake } from "radash"
import ReactMarkdown from "react-markdown"
import { StrapiWeapon } from "../types"
import { capitalizeAll } from "../utils/stringUtils"
import { SimplePortrait } from "./Portrait"

const imgHost = process.env.STRAPI_URL

const WeaponCard = ({ weapon }: { weapon: StrapiWeapon }) => {
  return (
    <div id={`${snake(weapon.attributes.name.toLowerCase())}`}>
      <h3 className="text-2xl mb-1">
        {capitalizeAll(weapon.attributes.name, true)}
      </h3>
      <span className="text-lg">
        {capitalizeAll(weapon.attributes.type.toLowerCase())}{weapon.attributes.is5Star ? ' ⭐⭐⭐⭐⭐' : ''}
      </span>
      <div className="md:flex md:flex-row py-6">
        <div className="basis-1/12 mt-1">
          <a href={`/weapons#${snake(weapon.attributes.name.toLowerCase())}`} style={{ cursor: 'pointer' }}>
            <SimplePortrait
              src={
                weapon.attributes.img ??
                `${imgHost}${weapon.attributes.imgCMS?.data?.attributes?.url}`
              }
              size={72}
              className="bg-slate-200 dark:bg-slate-800"
            />
          </a>
        </div>
        <div className="basis-4/12">
          <h5 className="text-xl font-semibold">
            Statistics
          </h5>
          <p>Base ATK: {weapon.attributes.baseATKInitial ?? '-'} (lv. 1) &rarr; {weapon.attributes.baseATKFinal ?? '-'} (lv. 90)</p>
          <div>Substat Type: {weapon.attributes.subStatType ?? '-'}</div>
          <p>Substat Value: {weapon.attributes.subStatValInitial ?? '-'} (lv. 1) &rarr; {weapon.attributes.subStatValFinal ?? '-'} (lv. 90)</p>
          <p></p>
        </div>
        <div className="basis-7/12">
          <h5 className="text-xl font-semibold">Passive effect</h5>
          <ReactMarkdown>
            {weapon.attributes.passiveEffect ?? ''}
          </ReactMarkdown>
        </div>
      </div>
    </div>
  )
}

export default WeaponCard