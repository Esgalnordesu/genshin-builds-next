import { FC } from "react";

const AdaptiveBox: FC<{ children?: JSX.Element | JSX.Element[] }> = ({ children }) => {
  return (
    <div className="bg-slate-100 dark:bg-slate-700 p-8 md:mx-8 mb-6 rounded-2xl min-h-32">
      {children}
    </div>
  )
}

export default AdaptiveBox