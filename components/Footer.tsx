import Link from "next/link"
import { ThemeToggler } from "./ThemeToggler"

const Footer = () => {
  return (
    <footer className="p-4 bg-slate-50 dark:bg-slate-800 shadow md:flex md:items-center md:justify-between md:p-6">
      <span className="text-sm dark:text-slate-200 sm:text-center">
        2022&nbsp;
        <a href="https://genshinjikken.fun/" className="hover:underline">GenshinJikken™
        </a>
      </span>
      <ul className="flex flex-wrap items-center mt-3 text-sm dark:text-slate-200 sm:mt-0">
        <li>
          <Link href="/about" className="mr-4 hover:underline md:mr-6 cursor-pointer">About</Link>
        </li>
        <li>
          <ThemeToggler />
        </li>
      </ul>
    </footer>
  )
}

export default Footer